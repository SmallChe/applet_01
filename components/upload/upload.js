// pages/readActivity/readActivity.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgList: [],
    uploadImgList:[]
  },
  selectImg() {
    wx.chooseImage({
      count: 6, // 默认9
      sizeType: ["original", "compressed"], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ["album", "camera"], // 可以指定来源是相册还是相机，默认二者都有
      success: res => {
        let that=this
        let uploadImg=res.tempFilePaths
        wx.showLoading({
          title:'图片加载中',
          mask:true
        })
        let imglength=uploadImg.length
        let curlength=0
        for(let i=0;i<uploadImg.length;i++){
          wx.uploadFile({
            header: {
              "token": wx.getStorageSync('token'),
              // "XX-Device-Type": "wxapp"
            },
            url: app.globalData.baseUrl + "/dangyuan/upload/one",
            filePath: uploadImg[i],
            name: "file", //这里根据自己的实际情况改
            success: res => {
              
              let path = JSON.parse(res.data).data.url;
              let list=that.data.imgList
              list.push(path)
              that.setData({
                imgList:list
              })
              curlength++;
            },
            fail: res => {
              curlength++;
              console.log(res);
            },
            complete: () => {
              if(curlength==imglength){
                wx.hideLoading();
              }
              
            }
          });
        }

      }
    });
  },
  previewImage(e) {
    var that = this;
    //获取当前图片的下标
    var index = e.currentTarget.dataset.index;
    //所有图片
    var imgList = that.data.imgList;
    //预览图片
    wx.previewImage({
      //当前显示图片
      current: imgList[index],
      //所有图片
      urls: imgList
    })
  },
  deleteImage(e) {
    var that = this;
    var imgList = that.data.imgList;
    var index = e.currentTarget.dataset.index;
    imgList.splice(index, 1);
    // uploadImgList.splice(index, 1);
    this.setData({
      imgList: imgList
    })
  },
  getFiles: function () {
    return this.data.imgList;
  }
})
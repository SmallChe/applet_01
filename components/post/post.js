// components/studyitem/studyitem.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    //接收数据的名称，数据的名称交做card
    card: {
      type: Object,
      value:{
        title:'不忘初心、牢记使命',
        img:'/static/images/user/dang.jpg',
        excerpt:'学校"不忘初心，牢记使命"主体教育召开工作培训会',
        time:'2019-11-22 19:26:59'
      }
    },
    infoText: {
      type: Object,
      value: {
        title: '党员素质肖像小程序上线了',
        img: '/static/images/user/bg.jpg',
        excerpt: '《实施意见》)和《教育部办公厅关于开展新时代高校党建示范创建和质量创优工作的通知》(教思政厅函〔2018〕23号,以下简称《双创工作通知》)安排和评审工作方案,经资格审查、专家通讯评审、教育部党建工作领导小组成员单位集中审议、结果公示,遴选产生10个高校党委、100个院系党组织、559个党支部分别作为全国党建工作示范高校、标杆院系、样板支部培育创建单位(名单见附件 2、3)……',
        time: '2019-11-11 11:26:59'
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
      goToNews(){
        wx.navigateTo({
          url: '/pages/news/news',
        })
      }
  }
})

  Component({
    data: {
      selected: 0,
      color: "#7A7E83",
      selectedColor: "#3cc51f",
      list: [
        {
          pagePath: "pages/branch/branch",
          iconPath: "static/images/icon/organization.png",
          selectedIconPath: "/static/images/icon/organizationC.png",
          text: "支部1"
        },
        {
          pagePath: "pages/study/study",
          iconPath: "static/images/icon/study.png",
          selectedIconPath: "static/images/icon/studyC.png",
          text: "思政课堂"
        },
        {
          pagePath: "pages/index/index",
          iconPath: "static/images/icon/heart.png",
          selectedIconPath: "static/images/icon/heartC.png",
          text: "素质肖像"
        },
        {
          pagePath: "pages/activity/activity",
          iconPath: "static/images/icon/activity.png",
          selectedIconPath: "static/images/icon/activityC.png",
          text: "活动思政"
        },
        {
          pagePath: "pages/user/user",
          iconPath: "static/images/icon/me.png",
          selectedIconPath: "static/images/icon/meC.png",
          text: "我的"
        }
      ]
    },
    attached() {
    },
    methods: {
      switchTab(e) {
        const data = e.currentTarget.dataset
        console.log(data.path)
        return

        /*  const url = data.path
          wx.switchTab({url})
          this.setData({
            selected: data.index
          })*/
      }
    }
  })

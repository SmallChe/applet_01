// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scoreList: [
      {
        name: '目前总分',
        score: "20",
      },
      {
        name: '思想先进',
        score: "20",
      },
      {
        name: '专业过硬',
        score: "20",
      },
      {
        name: '乐于奉献',
        score: "20",
      }
    ],
  
    score: [
      {
        name: '思想先进',
        score: "+0",
      },
      {
        name: '专业过硬',
        score: "+0",
      },
      {
        name: '乐于奉献',
        score: "+0",
      }
    ],

    active: 0,
    navList: [
      {
        name: '领取积分',
        url: '/pages/dorm/stulife',
        icon: '/static/images/icon/jifen.png',
        url:'../gainPoint/gainPoint'
      },
      {
        name: '积分规则',
        url: '/pages/stulife/stulife',
        icon: '/static/images/icon/guize.png'
      },
      {
        name: '更换肖像',
        url: '/pages/stulife/stulife',
        icon: '/static/images/icon/photo.png'
      },
      {
        name: '积分奖励',
        url: '/pages/stulife/stulife',
        icon: '/static/images/icon/liwu.png'
      },
      {
        name: '分享肖像',
        url: '/pages/stulife/stulife',
        icon: '/static/images/icon/share.png'
      }
    ]
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
})


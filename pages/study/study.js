// pages/study/study.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    newsList: [{
      title: "标题一",
      infoText: "描述1"
    },
    {
      title: "标题二",
      infoText: "描述2"
    },
    {
      title: "标题三",
      infoText: "描述3"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showModal({
      title: '温馨提示',
      content: '自觉开展学习，每日第一次积两分，之后每次学习积一分。每日积分不超过10分',
      showCancel:false,
        
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
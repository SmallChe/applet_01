Page({

  /**
   * 页面的初始数据
   */
  data: {

    navList: [
      {
        name: '自觉学习',
        icon: '/static/images/portrait/getPoint.png',
        url: '../dorm/dorm'
      },
      {
        name: '三会一课',
        icon: '/static/images/portrait/zushi.png',
        url: '../dorm/dorm'
      },
      {
        name: '主题党日',
        icon: '/static/images/portrait/jiangdangke.png',
        url: '../dorm/dorm'
      },
      {
        name: '  读书活动',
        icon: '/static/images/portrait/dushu.png',
        url: '../dorm/dorm'
      },
    ],
    List: [
      {
        name: '个人荣誉',
        icon: '/static/images/portrait/geren.png',
        url: '../honor/honor'
      },
      {
        name: '认真上课',
        icon: '/static/images/portrait/renzhen.png',
        url: '../renzhenshangke/renzhenshangke'
      },
      {
        name: '技能竞赛',
        icon: '/static/images/portrait/jineng.png',
        url: '../competition/competition'
      },
      {
        name: '创新创业',
        icon: '/static/images/portrait/chuangxin.png',
        url: '../chuangxinchuangye/chuangxinchuangye'
      },
      {
        name: '  学术论文',
        icon: '/static/images/portrait/xueshu.png',
        url: '../paper/paper'
      },
    ],
    Array: [
      {
        name: '帮助学生学业',
        icon: '/static/images/portrait/point.png',
        url: '../dorm/dorm'
      },
      {
        name: '关心学生生活',
        icon: '/static/images/portrait/guanxin.png',
        url: '../dorm/dorm'
      },
      {
        name: '公益活动',
        icon: '/static/images/portrait/gongyi.png',
        url: '../dorm/dorm'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})
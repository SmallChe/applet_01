Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    gotime: 0,
    timeShow: false,
    teacherName: '',//教师名称
    assistant: '',//学生助理
    floor: '',//楼层
    number: '',//房号
    Bname: '',//被访问学生姓名
    situation: '',//走访情况
    currentDate: new Date().getTime(),
    minDate: new Date(2019, 0, 1).getTime(),
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      } else if (type === 'month') {
        return `${value}月`;
      }
      return value;
    }
  },
  //打开时间选择框
  openTimeSelecter() {
    this.setData({
      timeShow: true
    })
  },
  //设置data里面的选中时间
  onDateSelect(event) {
    this.setData({
      currentDate: event.detail
    });
    
  },
  //
  onTimeShowClose(selectTime) {
    this.setData({ timeShow: false })
    Date.prototype.format = function (fmt) {
      var o = {
        "M+": this.getMonth() + 1,                 //月份 
        "d+": this.getDate(),                    //日 
        "h+": this.getHours(),                   //小时 
        "m+": this.getMinutes(),                 //分 
        "s+": this.getSeconds(),                 //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds()             //毫秒 
      };
      if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
      }
      for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
      }
      return fmt;
    }
    let fmtTime = new Date(this.data.currentDate).format("yyyy-MM-dd")
    this.setData({ gotime: fmtTime })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  onLoad() {
    wx.showModal({
      title: '温馨提示',
      content: '录入“荣誉证书”信息并上传证书图片，每项国家级荣誉积40分、省级荣誉积30分、市级荣誉积20分、校级荣誉积10分',
      showCancel: false
    })
  
  }
})
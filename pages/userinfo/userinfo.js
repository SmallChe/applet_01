// pages/userinfo/userinfo.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      sid:'',
      name:'',
      statusName:'',
      typeName:'',
      mobile:'',
      gender:'',
      party:'',
      level:'',

  },
  onChangeSid(event){
    this.setDate({sid:event.detail})
  },
  onChangeName(event) {
    this.setDate({ name: event.detail })
  },

  onChangeStatusName(event) {
    this.setDate({ statusName: event.detail })
  },

  onChangeTypeName(event) {
    this.setDate({ typeName: event.detail })
  },

  onChangeMobile(event) {
    this.setDate({ mobile: event.detail })
  },

  onChangeGender(event) {
    this.setDate({ gender: event.detail })
  },

  onChangeParty(event) {
    this.setDate({ party: event.detail })
  },
  onChangeLevel(event) {
    this.setDate({ level: event.detail })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
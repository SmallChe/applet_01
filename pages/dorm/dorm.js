// pages/stulife/stulife.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    gotime: 0,
    timeShow: false,
    teacherName:'',//教师名称
    assistant: '',//学生助理
    floor: '',//楼层
    number: '',//房号
    Bname: '',//被访问学生姓名
    situation: '',//走访情况
    currentDate: new Date().getTime(),
    minDate: new Date(2019, 0, 1).getTime(),
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      } else if (type === 'month') {
        return `${value}月`;
      }
      return value;
    }
  },
  //修改教师名称的值
  onTeacherNameChange(e){
    this.setData({ teacherName:e.detail})
  },
  //修改楼层的值
  onFloorChange(e) {
    this.setData({ floor: e.detail })
  },
  //修改房间名称的值
  oneNumberChange(e) {
    this.setData({ number: e.detail })
  },
  //修改被访问称的值
  onBnameChange(e) {
    this.setData({ Bname: e.detail })
  },
  //修改走访情况的值
  onSituationChange(e) {
    this.setData({ situation: e.detail })
  },
  //打开时间选择框
  openTimeSelecter() {
    this.setData({
      timeShow: true
    })
  },
  //设置data里面的选中时间
  onDateSelect(event) {
    this.setData({
      currentDate: event.detail
    });
  },
  //
  onTimeShowClose(selectTime) {
    this.setData({ timeShow: false })
    Date.prototype.format = function (fmt) {
      var o = {
        "M+": this.getMonth() + 1,                 //月份 
        "d+": this.getDate(),                    //日 
        "h+": this.getHours(),                   //小时 
        "m+": this.getMinutes(),                 //分 
        "s+": this.getSeconds(),                 //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds()             //毫秒 
      };
      if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
      }
      for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
      }
      return fmt;
    }
    let fmtTime = new Date(this.data.currentDate).format("yyyy-MM-dd")
    this.setData({ gotime: fmtTime })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showModal({
      title: '温馨提示',
      content: '党员教师走访学生宿舍，关心学生生活，帮助学生解决实际问题，做好记录并上传至小程序，每次积5分。学生党员主动协助教师党员走访宿舍，做好记录并及时上传资料，每次积5分。学生党员主动协助教师党员走访宿舍，做好记录并及时上传资料，每次积5分。',
      showCancel: false,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
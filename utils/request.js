var Fly = require("./fly")
let baseUrl = require("../app.js").baseUrl

const fly = new Fly
//添加全局配置、拦截器等
//添加请求拦截器
fly.interceptors.request.use((request) => {

    request.headers = {
        // "X-Tag": "flyio",
        'content-type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache',
        // 'XX-Token': wx.getStorageSync('token'),
        'token': wx.getStorageSync('token'),
        // 'XX-Device-Type': 'wxapp',
        // 'XX-Wxapp-AppId': 'wx5970cd24111b86e7',
        'appid': 'wx5970cd24111b86e7',
        // 'content-type': 'application/json'
    };


    // console.log('请求token:'+store.state.token)

    // request.headers['token']= 'abcdef'

    let authParams = {
        //公共参数
        /* "categoryType": "SaleGoodsType@sim",
         "streamNo": "wxapp153570682909641893",
         "reqSource": "MALL_H5",
         "appid": "string",
         "timestamp": new Date().getTime(),
         "sign": "string"*/
    };

    request.body && Object.keys(request.body).forEach((val) => {
        if (request.body[val] === "") {
            delete request.body[val]
        }
        ;
    });
    request.body = {
        ...request.body,
        ...authParams
    }
    return request;
});
//添加响应拦截器
fly.interceptors.response.use(
    (response) => {
        console.log(response)
        if (response.data.errno !== 0) {
            //拦截到请登录时操作
            if (response.data.errno == 1 || response.data.msg == '请登录') {
                //获取权限
                //登录获取code
                let isAuth = wx.getStorageSync('isAuth')







                
                //判断是否已经授权
                // if (isAuth === true) {
                //     let isdologin = wx.getStorageSync('isdologin')
                //     if (isdologin == '' || isdologin == false) {
                //         wx.setStorageSync('isdologin', true)
                //         // console.log('页面', getCurrentPages())
                //         let that = this
                //         wx.login({
                //             success: loginRes => {
                //                 console.log(loginRes)
                //                 if (loginRes.code) {
                //                     wx.getUserInfo({
                //                         withCredentials: true,
                //                         success: res => {
                //                             //  console.log(res)
                //                             //wx.setStorageSync('hasGetUserInfo', '1');
                //                             fly.post('/one/login/wxapp', {
                //                                 code: loginRes.code,
                //                                 encryptData: res.encryptedData,
                //                                 iv: res.iv,
                //                                 raw_data: res.rawData,
                //                                 signature: res.signature
                //                             })
                //                                 .then(function (response) {
                //                                     console.log(response)
                //                                     wx.setStorageSync('login', '1');
                //                                     wx.setStorageSync('token', response.data.token);
                //                                     fly.get('/dangyuan/user/getUserInfo').then(res => {
                //                                         wx.setStorageSync('user', res.data);
                //                                         if (getCurrentPages().length != 0) {
                //                                             //刷新当前页面的数据
                //                                             getCurrentPages()[getCurrentPages().length - 1].onLoad()
                //                                         }
                //                                         wx.setStorageSync('isdologin', false)
                //                                     })
                //                                 })
                //                         }
                //                     });


                //                 }
                //             },
                //         });
                //     }
                // }








            } else {
                wx.showToast({
                    title: response.data.msg,
                    icon: 'none',
                    duration: 1000
                })
            }

        }
        //如果请求头里面有设置session id、 就把他保存下来到本地
        // console.log(response)
        return response.data;//请求成功之后将返回值返回
    },
    (err) => {
        //请求出错，根据返回状态码判断出错原因
        console.log('请求出错：');
        console.log(err);
        // wx.hideLoading();
        if (err) {
            return "请求失败";
        }
        ;
    }
);

fly.config.baseURL = baseUrl;
// fly.config.baseURL = 'http://192.168.31.120:8080';
export default fly;
